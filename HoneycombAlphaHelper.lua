-- HoneycombAlphaHelper - a Honeycomb Alpha Linux driver
-- Repository:	https://gitlab.com/dpeukert/honeycomb-xplane-linux
-- Author:		Daniel Peukert
-- Verion:		0.0.2
-- License:		GNU GPLv3

-- Commands for switching all generators on and off at once
local generators = dataref_table('sim/cockpit2/electrical/generator_on')

function generators_all(state)
	for i = 0, 7 do
		generators[i] = state
	end
end

create_command(
	'dpeukert/HoneycombAlphaHelper/generators_on',
	'Generators all on.',
	'generators_all(1)',
	'',
	''
)

create_command(
	'dpeukert/HoneycombAlphaHelper/generators_off',
	'Generators all off.',
	'generators_all(0)',
	'',
	''
)

-- Commands for switching all batteries on and off at once
local batteries = dataref_table('sim/cockpit2/electrical/battery_on')

function batteries_all(state)
	for i = 0, 7 do
		batteries[i] = state
	end
end

create_command(
	'dpeukert/HoneycombAlphaHelper/batteries_on',
	'Batteries all on.',
	'batteries_all(1)',
	'',
	''
)

create_command(
	'dpeukert/HoneycombAlphaHelper/batteries_off',
	'Batteries all off.',
	'batteries_all(0)',
	'',
	''
)

-- Commands for switching all magnetos to left and right at once
local magnetos = dataref_table('sim/cockpit2/engine/actuators/ignition_on')

function magnetos_all(state)
	for i = 0, 7 do
		magnetos[i] = state
	end
end

create_command(
	'dpeukert/HoneycombAlphaHelper/magnetos_left',
	'Magnetos all left.',
	'magnetos_all(1)',
	'',
	''
)

create_command(
	'dpeukert/HoneycombAlphaHelper/magnetos_right',
	'Magnetos all right.',
	'magnetos_all(2)',
	'',
	''
)
