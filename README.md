# Honeycomb Alpha + Bravo X-Plane Linux Driver

Due to the (as of now) non-existent support for Linux by Honeycomb, I've decided to implement all the functionality that was missing or wasn't working as I wanted in the form of two FlyWithLua scripts.

Writing these scripts was my first foray into both Lua programming and programming for X-Plane in general, so if you run into issues, it's probably my fault. I've tested the scripts on an up-to-date Arch Linux install with X-Plane 11.55.

## Implemented functionality

### HoneycombAlphaHelper

* bindable commands for turning all generators on and off (X-Plane only supports controlling each generator separately)
  * `dpeukert/HoneycombAlphaHelper/generators_on` - Generators all on
  * `dpeukert/HoneycombAlphaHelper/generators_off` - Generators all off
* bindable commands for turning all batteries on and off (X-Plane only supports controlling each battery separately)
  * `dpeukert/HoneycombAlphaHelper/batteries_on` - Batteries all on
  * `dpeukert/HoneycombAlphaHelper/batteries_off` - Batteries all off
* bindable commands for turning all magnetos left and right (X-Plane only supports turning all magnetos on and off)
  * `dpeukert/HoneycombAlphaHelper/magnetos_left` - Magnetos all left
  * `dpeukert/HoneycombAlphaHelper/magnetos_right` - Magnetos all right

### HoneycombBravoHelper

* all LEDs now correspond to their counterparts in-game
* increasing and decreasing autopilot values is now possible with bindable commands for the various modes and the rotary encoder
  * `dpeukert/HoneycombBravoHelper/mode_ias` - Set autopilot rotary encoder mode to IAS
  * `dpeukert/HoneycombBravoHelper/mode_crs` - Set autopilot rotary encoder mode to CRS
  * `dpeukert/HoneycombBravoHelper/mode_hdg` - Set autopilot rotary encoder mode to HDG
  * `dpeukert/HoneycombBravoHelper/mode_vs` - Set autopilot rotary encoder mode to VS
  * `dpeukert/HoneycombBravoHelper/mode_alt` - Set autopilot rotary encoder mode to ALT
  * `dpeukert/HoneycombBravoHelper/increase` - Increase the value of the autopilot mode selected with the rotary encoder
  * `dpeukert/HoneycombBravoHelper/decrease` - Decrease the value of the autopilot mode selected with the rotary encoder
* bindable commands for enabling (= not setting to maximum thrust) thrust reversers (all or for a specific engine) while the command is active (X-Plane only supports toggling or setting to maximum thrust and only separately for each engine)
  * `dpeukert/HoneycombBravoHelper/thrust_reversers` - Hold all thrust reversers on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_1` - Hold thrust reverser \#1 on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_2` - Hold thrust reverser \#2 on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_3` - Hold thrust reverser \#3 on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_4` - Hold thrust reverser \#4 on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_5` - Hold thrust reverser \#5 on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_6` - Hold thrust reverser \#6 on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_7` - Hold thrust reverser \#7 on
  * `dpeukert/HoneycombBravoHelper/thrust_reverser_8` - Hold thrust reverser \#8 on

## Installation and configuration

[FlyWithLua](https://forums.x-plane.org/index.php?/files/file/38445-flywithlua-ng-next-generation-edition-for-x-plane-11-win-lin-mac/) must be installed.

1. Move `HoneycombAlphaHelper.lua` and/or `HoneycombBravoHelper.lua` to `$YOUR_XPLANE_FOLDER/Resources/plugins/FlyWithLua/Scripts/`
2. Assign the commands to controls on the Alpha and Bravo
   * Alpha
     * Assign each position of the MASTER ALT switch to the `Generators all on` and `Generators all off` commands
     * Assign each position of the MASTER BAT switch to the `Batteries all on` and `Batteries all off` commands
     * Assign the L and R positions of the ignition switch to the `Magnetos all left` and `Magnetos all right` commands
   * Bravo
     * Assign each position of the left rotary encoder to its corresponding `Set autopilot rotary encoder mode to X` command
     * Assign both directions of the right rotary encoder to the `Increase`/`Decrease the value of the autopilot mode selected with the rotary encoder` commands
     * Assign either the `Hold all thrust reversers on` command or the corresponding engine-specific `Hold thrust reverser #X on` command to the reverser levers of each engine
3. Enjoy!

## Troubleshooting

### The LEDs on my Bravo aren't working!

Your `Log.txt` might contain this error: `HoneycombBravoHelper | Error: Unable to connect to the Honeycomb Bravo throttle quadrant`. This can be caused by insufficient permissions for the Bravo, which result in the script being unable to send commands to the Bravo's LEDs.

A udev rule (`52-HoneycombBravo.rules`) that sets the Bravo's permissions to 666 is included with the scripts, try adding it to your udev rule folder (`/usr/lib/udev/rules.d/` on my machine) and see if that helps.

### Something else?

Let me know and I'll try to figure out what's wrong :)

## License
The contents of this repository are provided under the GPLv3 license or any later version (SPDX identifier: `GPL-3.0-or-later`).
